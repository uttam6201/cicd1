#!/usr/bin/env python
# coding: utf-8

# In[4]:
import openfst_python as fst
import numpy as np
import pandas as pd
import tensorflow as tf
import pickle
from tensorflow.keras.models import Model,load_model
from math import log
from numpy import array
from numpy import argmax
import sys
import subprocess
# In[7]:
import os
import math
import random

def get_random_coordinate_for_char(ch, key_coord):
    asc_ch = ord(ch)
    ls = key_coord[asc_ch]

#     if ch == 'n':
#         x_movement = 20
#         y_movement = 20

#     else:

    x_movement = np.random.randint(1, 100)
    y_movement = np.random.randint(1, 100)
#

    x_low, y_low, x_high, y_high = ls[2], ls[3], ls[4], ls[5]

    x = (x_low + (((x_high-x_low) * x_movement) / 100))
    y = (y_low + (((y_high-y_low) * y_movement) / 100))

    return x, y

def get_coordinates_for_word(word, key_coord):
    x_coordinates = []
    y_coordinates = []

    for ch in word:
        x, y = get_random_coordinate_for_char(ch, key_coord)

        x_coordinates.append(x)
        y_coordinates.append(y)

    return x_coordinates, y_coordinates

def convert_coordinates_to_np_array(x, y):
    x_y = list(zip(x, y))

    x_y = list(map(list, x_y))

    x_y = np.asarray(x_y).astype('float32')

    var = np.zeros((15, 2))
    for i in range(x_y.shape[0]):
        var[i, :] = x_y[i]

    return var


# In[22]:

model = load_model('error_bilstm_model_poc_1.h5')
print("ai model loaded")
with open('vocab_x.pickle', 'rb') as handle:
    vocab_x = pickle.load(handle)

with open('vocab_y.pickle', 'rb') as handle:
    vocab_y = pickle.load(handle)

with open('key_coord_chars.pickle', 'rb') as handle:
    key_coord = pickle.load(handle)

def get_error_model_output(word):
    timestamp = 15
    features = 2

    ##load model


    ##load vocabs


    # input preparation
    coordinates = []
    x_coord = []
    y_coord = []
    x, y = get_coordinates_for_word(word, key_coord)
    x_coord.append(x)
    y_coord.append(y)
    coord = convert_coordinates_to_np_array(x, y)
    coordinates.append(coord)
    a = np.array(coordinates)

    #model inference
    y = model.predict(a)

    outputs = []

    for i in range(y.shape[0]):
        for j in range(min(y.shape[1], len(word))):
            index = np.argmax(y[i][j])

            if index == 0:
                continue
            else:
                outputs.append(y[i][j])

    return outputs


# In[23]:


#print(op)
# In[ ]:


#beam search apply to touch points to make literals

all_touch_points_data=[]
all_state={}
n_best={}
n_best_word={}
n_best_final={}
n_matched_words={}
vocab_char={}
reverse_char={}
vocab_word={}
reverse_vocab={}

f_lexicon = fst.Fst.read("lexicon_1.srt")
f_LM = fst.Fst.read("en-hi_1_1.pru.srt")

def beam_search_decoder(data, k):
    #print(data)

    count_state=1
    sequences = [[list(), 0.0]]
    # walk over each step in sequence
    for row in data:
        previous_state={}

        all_candidates = list()
        # expand each current candidate
        for i in range(len(sequences)):
            seq, score = sequences[i]
            for j in range(len(row)):
                t = row[j]
                if(t == 0):
                    t = 0.00000000000001
                candidate = [seq + [j], score - log(t)]
                #print(candidate)
                all_candidates.append(candidate)
        # order all candidates by score
        ordered = sorted(all_candidates, key=lambda tup:tup[1])
        # select k best
        sequences = ordered[:k]
        #print(sequences)
        with open('vocab_y.pickle', 'rb') as handle:
            vocab_y = pickle.load(handle)
        for sequence in sequences:
            word = ''
            ls = sequence[0]
            score = sequence[1]

            for index in ls:
                if index==0:
                    word+='_'
                else:


                    word += vocab_y[index]

            #print(word, score)
            all_state[word]=score
            previous_state[word]=count_state
            count_state=count_state+1
            if previous_state not in all_touch_points_data:

                all_touch_points_data.append(previous_state)
    return sequences

# In[ ]:
def top_best_C_L_LM(new_state,delta,f_C_L_LM):

    for arc in f_C_L_LM.arcs(new_state):
        #print(reverse_vocab[str(arc.olabel)],arc.nextstate,arc.weight)
        prev_state=new_state
        new_state=arc.nextstate
        if reverse_vocab[str(arc.olabel)]!="<epsilon>":
            #if len(reverse_vocab[str(arc.olabel)])>=len(sys.argv[1].split(" ")[-1])-1 and len(reverse_vocab[str(arc.olabel)])<=len(sys.argv[1].split(" ")[-1])+1:


            n_best[reverse_vocab[str(arc.olabel)]]=float(delta)+float(arc.weight)
            #print(n_best)

        else:

            delta=float(delta)+float(arc.weight)

            top_best_C_L_LM(new_state,delta,f_C_L_LM)

# state lexicon finder
def state_finder_lexicon(previous_state,text,f_lexicon):
    next_state=-1

    list_nstate={}
    for arc in f_lexicon.arcs(previous_state):
        if str(reverse_char[str(arc.ilabel)])==str(text):
            #print (previous_state, reverse_char[str(arc.ilabel)], reverse_vocab[str(arc.olabel)], arc.weight, arc.nextstate)
            next_state=arc.nextstate
            list_nstate[str(next_state)]= arc.weight
        if next_state!=-1:
            break
    return next_state,arc.weight
def top_best_lexicon(new_state,delta,text,f_lexicon):

    for arc in f_lexicon.arcs(new_state):
        #print(reverse_vocab[str(arc.olabel)])
        prev_state=new_state
        new_state=arc.nextstate
        if reverse_vocab[str(arc.olabel)]!="<epsilon>":


            #print(prev_state, reverse_char[str(arc.ilabel)], reverse_vocab[str(arc.olabel)], arc.weight, arc.nextstate)
            #print(reverse_vocab[str(arc.olabel)])
            if reverse_vocab[str(arc.olabel)] not in n_best and reverse_vocab[str(arc.olabel)][:len(text)]==text:
                n_best_final[reverse_vocab[str(arc.olabel)]]=n_best_word_all[reverse_vocab[str(arc.olabel)]]

        else:
            top_best_lexicon(new_state,delta,text,f_lexicon)
#final=beam_search_decoder(op,5)
def state_finder_next_word(previous_state,text,f_LM):
    next_state=-1

    list_nstate={}
    for arc in f_LM.arcs(previous_state):
        if str(arc.ilabel)==str(vocab_word[text]):
            #print (previous_state, arc.ilabel, arc.olabel, arc.weight, arc.nextstate)
            next_state=arc.nextstate
            list_nstate[str(next_state)]= arc.weight
        if next_state!=-1:
            break
    return next_state,arc.weight
def top_best_next_word(new_state,delta,f_LM):

    for arc in f_LM.arcs(new_state):
        if arc.ilabel !=0:
            if str(arc.ilabel) not in n_best:
                n_best_word[reverse_vocab[str(arc.ilabel)]]=float(delta)+float(arc.weight)
        else:
            top_best_next_word(arc.nextstate,float(arc.weight),f_LM)

def editDistance(str1, str2, m, n):
 
    # If first string is empty, the only option is to
    # insert all characters of second string into first
    if m == 0:
        return n
 
    # If second string is empty, the only option is to
    # remove all characters of first string
    if n == 0:
        return m
 
    # If last characters of two strings are same, nothing
    # much to do. Ignore last characters and get count for
    # remaining strings.
    if str1[m-1] == str2[n-1]:
        return editDistance(str1, str2, m-1, n-1)
 
    # If last characters are not same, consider all three
    # operations on last character of first string, recursively
    # compute minimum cost for all three operations and take
    # minimum of three values.
    return 1 + min(editDistance(str1, str2, m, n-1),    # Insert
                   editDistance(str1, str2, m-1, n),    # Remove
                   editDistance(str1, str2, m-1, n-1)    # Replace
                   )
 
with open("en-hi_final.syms", mode='r') as infile:
    reader = infile.readline()

    reader = reader.rstrip("\n\r")
    rows=reader.split("\t")
    temp=[i for i in rows[1:] if i and str(i)!="N"]

    if "{}" not in rows[0]:

        vocab_word[rows[0].lower()]=rows[1]
        reverse_vocab[rows[1]]=rows[0].lower()
    while reader:

        reader=infile.readline()

        reader = reader.rstrip("\n\r")
        rows=reader.split("\t")

        temp=[i for i in rows[1:] if i]
        try:
            if "{}" not in rows[0]:
                vocab_word[rows[0].lower()]=rows[1]
                reverse_vocab[rows[1]]=rows[0].lower()
            else:
                r=1
        except:
            r=1


            #print("no")
vocab_word_1=vocab_word

#print(n_best)
#reachable words from lexicon fst


with open("char.sym", mode='r') as infile:
    reader = infile.readline()

    reader = reader.rstrip("\n\r")
    rows=reader.split(" ")
    temp=[i for i in rows[1:] if i and str(i)!="N"]
    if "{}" not in rows[0]:

        vocab_char[rows[0].lower()]=rows[1]
        reverse_char[rows[1]]=rows[0].lower()
    while reader:

        reader=infile.readline()

        reader = reader.rstrip("\n\r")
        rows=reader.split(" ")

        temp=[i for i in rows[1:] if i]
        try:
            if "{}" not in rows[0]:
                vocab_char[rows[0].lower()]=rows[1]
                reverse_char[rows[1]]=rows[0].lower()
            else:
                r=1
        except:
            r=1
top_best_next_word(0,0,f_LM)
n_best_word_all=n_best_word
vocab_word={}
reverse_vocab={}
reverse_char={}
n_best_word={}

def intersection(lst1, lst2):
    lst3 = [value for value in lst1 if value in lst2]
    return lst3
def decoder(input,f_lexicon,f_LM):

    global n_best
    op = get_error_model_output(input.split(" ")[-1])


    with open('vocab_y.pickle', 'rb') as handle:
        vocab_y = pickle.load(handle)
        sequences = beam_search_decoder(op,10)
#print(all_touch_points_data)
    #print("-------------")
    for sequence in sequences:
        word = ''
        ls = sequence[0]
        score = sequence[1]

        for index in ls:
            try:

                word += vocab_y[index]
            except:
                continue
        #print(word, score)

#print(all_state)
    state_inc=1
    initial_state=0
    initial_state_map={}
    old_initial_state_map={}
    symbols={}

    f=open("c-fst.txt","w+")

#print(all_state)
    delete_list1=[]
    last_state=0
    for i in range(0,len(all_touch_points_data)):
        previous_delete=delete_list1
        delete_list1=[]   # deletion operation

        for j in all_touch_points_data[i]:

            if i==0:
                delete_list=[str(initial_state),str(state_inc),input[i],str(j),str(all_state[j])]
                txt=str(initial_state)+" "+str(state_inc)+" "+input[i]+" "+ str(j)+" "+str(all_state[j])
                initial_state_map[j]=state_inc
                state_inc=state_inc+1
            #print(txt)
                f.write(txt+'\n')
                symbols[input[i]]=1
            else:
                for k in old_initial_state_map:
                    if k==j[:-1]:
                        delete_list=[str(old_initial_state_map[k]),str(state_inc),str(j[-2])+"_"+input[i],str(j[-1]),str(all_state[j])]

                        symbols[str(j[-2])+"_"+input[i]]=1
                        txt=str(old_initial_state_map[k])+" "+str(state_inc)+" "+str(j[-2])+"_"+input[i]+" "+ str(j[-1])+" "+str(all_state[j])
                        initial_state_map[j]=state_inc
                        state_inc=state_inc+1
                    #print(txt)
                        f.write(txt+'\n')
                        last_state=str(state_inc)

            delete_list1.append(delete_list)
        old_initial_state_map=initial_state_map
        initial_state_map={}
    #print(previous_delete,delete_list1)
        if i>1 and len(input.split(" ")[-1])>1:
            for i1 in previous_delete:
                for i2 in delete_list1:
                    if i1[1]==i2[0]:
                        txt=str(i1[0])+" "+str(i2[1])+" "+"_"+str(i2[2])+" "+ str(i2[3])+" "+str(float(i2[4])+float(i1[4])+30.00001)
                        f.write(txt+'\n')
                        symbols["_"+str(i2[2])]=1


#print(all_touch_points_data)
    for k in all_touch_points_data[len(all_touch_points_data)-1]:
        txt=str(all_touch_points_data[len(all_touch_points_data)-1][k])+" "+str(int(last_state))+" "+"<w>"+" "+"<w>"
        f.write(txt+'\n')
    symbols["<w>"]=1
    l2={}
    for k in all_touch_points_data:
        for j in k:
            l2[j]=k[j]

    if len(input.split(" ")[-1])>1:
        for k in l2:
            for j in ["a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z"]:

                txt=str(l2[k])+" "+str(l2[k])+" "+j+" "+j+" "+str(20.677676767)
                f.write(txt+'\n')
                symbols[j]=1
    f.write(str(int(last_state))+'\n')
    f.close()
    f=open("input_symbol_c-fst.txt","w+")
    count_symbols=0
#print(symbols)

    for i in symbols:
        f.write(i+"\t"+str(count_symbols)+"\n")
        count_symbols=count_symbols+1
    f.close()
    os.system("fstcompile --isymbols=input_symbol_c-fst.txt --osymbols=char.sym --keep_isymbols --keep_osymbols c-fst.txt C.fst ")
    os.system("fstarcsort C.fst C_sort.fst")
    os.system("fstcompose C_sort.fst lexicon_1.srt C_L.fst")
    os.system("fstcompose C_L.fst en-hi_1_1.pru.srt C_L_LM.fst")
    os.system("fstproject --project_output=true C_L_LM.fst result.fsa")
    os.system("fstshortestpath --nshortest=30 --unique=true  result.fsa shortest.fst")
    os.system("fstdraw --isymbols=en-hi_final.syms --osymbols=en-hi_final.syms -portrait shortest.fst | dot -Tps >shortest.ps ")
    f_C_L_LM = fst.Fst.read("shortest.fst")


# composition done
#------------------------------------------------------------------------
#Reading decoder output
    with open("en-hi_final.syms", mode='r') as infile:
        reader = infile.readline()

        reader = reader.rstrip("\n\r")
        rows=reader.split("\t")
        temp=[i for i in rows[1:] if i and str(i)!="N"]

        if "{}" not in rows[0]:

            vocab_word[rows[0].lower()]=rows[1]
            reverse_vocab[rows[1]]=rows[0].lower()
        while reader:

            reader=infile.readline()

            reader = reader.rstrip("\n\r")
            rows=reader.split("\t")

            temp=[i for i in rows[1:] if i]
            try:
                if "{}" not in rows[0]:
                    vocab_word[rows[0].lower()]=rows[1]
                    reverse_vocab[rows[1]]=rows[0].lower()
                else:
                    r=1
            except:
                r=1


            #print("no")
    try:
            
        top_best_C_L_LM(0,0,f_C_L_LM)
    except:
        n_best={}   

    
    #print("C_L_LM shortest-path output:",sorted(n_best,key=n_best.get,reverse=False)[:3])
    n_best_1 = {x:n_best[x] for x in n_best
                              if editDistance(input.split(" ")[-1],x[:len(input.split(" ")[-1])+1],len(input.split(" ")[-1]),len(x[:len(input.split(" ")[-1])+1]))<=2}

    #print("n_best_1",n_best_1)
    with open("char.sym", mode='r') as infile:
        reader = infile.readline()

        reader = reader.rstrip("\n\r")
        rows=reader.split(" ")
        temp=[i for i in rows[1:] if i and str(i)!="N"]
        if "{}" not in rows[0]:

            vocab_char[rows[0].lower()]=rows[1]
            reverse_char[rows[1]]=rows[0].lower()
        while reader:

            reader=infile.readline()

            reader = reader.rstrip("\n\r")
            rows=reader.split(" ")

            temp=[i for i in rows[1:] if i]
            try:
                if "{}" not in rows[0]:
                    vocab_char[rows[0].lower()]=rows[1]
                    reverse_char[rows[1]]=rows[0].lower()
                else:
                    r=1
            except:
                r=1


    sent=input.split(" ")
#print(sent)
    text=input
    res={}
    for j in sorted(n_best,key=n_best.get,reverse=False)[:3]:
        for key in n_best_word_all:
            if key.startswith(j):
                n_best_final[key]=n_best_word_all[key]
  
    #C_L_LM best candiddates
    score1={key:n_best[key] for key in sorted(n_best,key=n_best.get,reverse=False)[:3]}
    #print(score1)
            #print("no")
#print(len(n_best))
    #print("reachable output label:",sorted(n_best_final,key=n_best_final.get,reverse=False))
#print(n_best_final)
# reachable words look up in LM fst
#print("reachable words look up in LM fst")

    global n_best_word
            #print("no")
    if len(input.split(" "))==1:
        n_best_word=n_best_word_all
        
    else:
        intial_state=0
        for i in sent[:-1]:
            new_state,weight=state_finder_next_word(intial_state,i,f_LM)
            #print(new_state)

            if new_state== int(-1):
                new_state,weight=state_finder_next_word(0,i,f_LM)

            intial_state=int(new_state)
        top_best_next_word(new_state,0,f_LM)

    #print(sorted(n_best_final,key=n_best_final.get,reverse=False)[:5])
    final_dict = {x:n_best_word[x] for x in n_best_word 
                              if x in n_best_final}
    top_3_best=sorted(n_best,key=n_best.get,reverse=False)[:3]
    # applying edit distance:


    edit_distance_1 = {x:final_dict[x] for x in final_dict
                              if editDistance(top_3_best[0],x[:len(top_3_best[0])+1],len(top_3_best[0]),len(x[:len(top_3_best[0])+1]))==1}
    #print(sorted(edit_distance_1,key=edit_distance_1.get,reverse=False)[:3])
    #scle score2 in score scale
    score2={key:(edit_distance_1[key]/edit_distance_1[sorted(edit_distance_1,key=edit_distance_1.get,reverse=False)[:3][-1]])*n_best[sorted(n_best,key=n_best.get,reverse=False)[:3][-1]] for key in sorted(edit_distance_1,key=edit_distance_1.get,reverse=False)[:3]}
    #print(score2)

    if len(input.split(" ")[-1])>3:

        ds1 = set(sorted(score1,key=score1.get,reverse=False)[:3]+sorted(score2,key=score2.get,reverse=False)[:3])
    else:
        ds1 = set(sorted(score1,key=score1.get,reverse=False)[:1]+sorted(score2,key=score2.get,reverse=False)[:3])
    d = {}
    for k in ds1:
        if k in score1 and k in score2:

            d[k] = min(score1[k],score2[k])
        elif k in score1:
            d[k]=  score1[k]
        else:
            
            d[k]=score2[k]
    #print("next word prediction:",sorted(n_best_word,key=n_best_word.get,reverse=False)[:3])
    #print(d)
    n_matched_words=d
#print(n_matched_words)
    n_matched_words[sent[-1]]=0
    #print(sorted(n_matched_words,key=n_matched_words.get,reverse=False)[:3])

    return sorted(n_matched_words,key=n_matched_words.get,reverse=False)[:3]



def outputer_prediction(inp):


    k_best=[]
    proc = subprocess.Popen(['python3','fst_based_next_word_decoder_final.py','en-hi_final.syms','en-hi_1_1.pru.srt',inp],stdout=subprocess.PIPE)
    for line in proc.stdout:
        b1=line.rstrip().decode("utf-8").split("'")
        #print(b1)
        for r in b1:
            k_best.append(r)
    return k_best
def accuracy_check_suggestions_plus_predictions(inp):
    global n_best
    global n_best_word
    global n_best_final
    global n_matched_words
    global all_touch_points_data
    global reverse_vocab
    global reverse_char
    global vocab_word
    temp=inp.split(" ")
    suggestion_tape_saved=0
    prediction_tape_saved=0
    total_tape=0
    tape_saved=0
    for p in temp:
        total_tape=total_tape+len(p)
    str1=""
    for i in range(0,len(temp)):
        #if str1==inp:
         #   break
        if i>0:
            prediction=outputer_prediction(str1)
            if temp[i] in prediction:
                tape_saved=tape_saved+len(temp[i])-1
                prediction_tape_saved=prediction_tape_saved+len(temp[i])-1
                str1=str1+" "+temp[i]
                continue
            else:
                str1=str1+" "

        for j in range(0,len(temp[i])):
            str1=str1+temp[i][j]
            

            if len(str1.split(" ")[-1])<len(temp[i]):

                out=decoder(str1,f_lexicon,f_LM)
                #print(out)

                n_best={}
                n_best_word={}
                n_best_final={}
                n_matched_words={}
                all_touch_points_data=[]
                reverse_vocab={}
                reverse_char={}
                vocab_word={}
            else:
                out=[]
            print(str1,"current_word:",str1.split(" ")[-1],"output:",out)



            #print(out)
            if temp[i] in out:
                tape_saved=tape_saved+len(temp[i])-(j+1)-1
                suggestion_tape_saved=suggestion_tape_saved+len(temp[i])-(j+1)-1
                #print("tape_saved",i,len(i)-(j+1))
                str1=str1+temp[i][j+1:]
                break

    return total_tape,tape_saved,suggestion_tape_saved,prediction_tape_saved

r1=0
m1=0
s1=0
p1=0
sens={}
with open(sys.argv[1], mode='r') as infile:
    reader = infile.readline()
    reader = reader.rstrip("\n\r")
    rows=reader
    sens[rows]=1
    while reader:
        reader=infile.readline()
        reader = reader.rstrip("\n\r")
        rows=reader
        try:
            sens[rows]=1
        except:
            r=1

for s in sens:
    v1,v2,v3,v4=accuracy_check_suggestions_plus_predictions(s)
    r1=r1+v1
    m1=m1+v2
    s1=s1+v3
    p1=p1+v4
print("suggestions+prediction tape saved:",(m1/r1)*100,"%")
print("suggestions tape saved:",(s1/r1)*100,"%")
print("predictions tape savde:",(p1/r1)*100,"%")

auto_correct={} 
#print(vocab_word_1)                       
with open(sys.argv[2], mode='r') as infile:
    reader = infile.readline()

    reader = reader.rstrip("\n\r")

    if reader.split(",")[1] in vocab_word_1 : 
        auto_correct[reader.split(",")[0].lower()]=reader.split(",")[1].lower()
    while reader:

        reader=infile.readline()

        reader = reader.rstrip("\n\r")
        try:
            if reader.split(",")[1] in vocab_word_1 : 
                auto_correct[reader.split(",")[0].lower()]=reader.split(",")[1].lower()
        except:
            pass


def accuracy_check_auto_correct(inp,data):
    global n_best
    global n_best_word
    global n_best_final
    global n_matched_words
    global all_touch_points_data
    global reverse_vocab
    global reverse_char
    global vocab_word
   
    temp=inp
    
    out=decoder(temp,f_lexicon,f_LM)
    n_best={}
    n_best_word={}
    n_best_final={}
    n_matched_words={}
    all_touch_points_data=[]
    reverse_vocab={}
    reverse_char={}
    vocab_word={}
   
    flag=0
    if data in out:
        flag=1
        print(temp,":",out,":",flag)
        return 1
    print(temp,":",out,":",flag)

    return 0
v1=0
v2=0
print("auto-correct_report")

for s in sorted(auto_correct,reverse=False):
    #print(s)
        v2=v2+1
        v1=v1+accuracy_check_auto_correct(s,auto_correct[s])
print("auto-correct:",(v1/v2)*100,"%")